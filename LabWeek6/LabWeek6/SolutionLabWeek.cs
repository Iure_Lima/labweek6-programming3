using System.Diagnostics;

namespace LabWeek6;

public class SolutionLabWeek
{
    private int[] _tasks;
    
    public SolutionLabWeek()
    {
        // List of tasks
        _tasks = new int[]{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
    }

    public void SequentialBatchComputing()
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        Console.WriteLine("Sequential Batch Computing");
        for (int i = 0; i < _tasks.Length; i++)
        {
            Console.WriteLine($"Processing task: {_tasks[i]}");
            Thread.Sleep(1000); // Simulates a processing time for the task
        }
        stopwatch.Stop();
        
        Console.WriteLine($"Sequential processing finished, time:{stopwatch.ElapsedMilliseconds / 1000} seconds");
    }

    public void ThreadBatchComputing()
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        List<int> separationOfTasksList = new List<int>();
        List<int> separationOfTasksList2 = new List<int>();

        for (int i = 0; i < _tasks.Length; i++)
        {
            if (i < 10) separationOfTasksList.Add(_tasks[i]);
            else separationOfTasksList2.Add(_tasks[i]);
        }
        
        Console.WriteLine("Thread Batch Computing");
        Thread thread1 = new Thread(() => PrintListOfTasks(separationOfTasksList));
        Thread thread2 = new Thread(() => PrintList2OfTasks(separationOfTasksList2));
        
        thread1.Start();
        thread2.Start();
        thread2.Join();
        stopwatch.Stop();
        Console.WriteLine($"Thread processing finished, time:{stopwatch.ElapsedMilliseconds / 1000} seconds");
    }

    private static void PrintListOfTasks(List<int> list)
    {
        list.ForEach(e =>
        {
            Console.WriteLine($"Processing task: {e}");
            Thread.Sleep(1000); // Simulates a processing time for the task
        });
       
    }
    
    private static void PrintList2OfTasks(List<int> list)
    {
        list.ForEach(e =>
        {
            Console.WriteLine($"Processing task: {e}");
            Thread.Sleep(1000); // Simulates a processing time for the task
        });
    }
}